/// <reference path='../_all.ts' />

module bets {
    export interface IBetStorage {
	get(): ng.IHttpPromise<{entries: BetItem[]}>;
	put(bets: BetItem[]);
    }
}