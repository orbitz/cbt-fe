/// <reference path='../_all.ts' />

module bets {
    export interface IBetScope extends ng.IScope {
	bets: BetItem[];
	newBet: string;
	editedBet: BetItem;
	remainingCount: number;
	resolvedCount: number;
	allChecked: boolean;
	statusFilter: (value: BetItem, index: number) => boolean;
	location: ng.ILocationService;
	vm: BetCtrl;
    }
}