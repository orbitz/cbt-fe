declare module Backbone {
    export class Model {
        constructor (attr? , opts? );
        get(name: string): any;
        set(name: string, val: any): void;
        set(obj: any): void;
        save(attr? , opts? ): void;
        destroy(): void;
        bind(ev: string, f: Function, ctx?: any): void;
        toJSON(): any;
    }
    export class Collection<T> {
        constructor (models? , opts? );
        bind(ev: string, f: Function, ctx?: any): void;
        length: number;
        create(attrs, opts? ): any;
        each(f: (elem: T) => void ): void;
        fetch(opts?: any): void;
        last(): T;
        last(n: number): T[];
        filter(f: (elem: T) => boolean): T[];
        without(...values: T[]): T[];
	add(values: T[]): void;
    }
    export class View {
        constructor (options? );
        $(selector: string): JQuery;
        el: HTMLElement;
        $el: JQuery;
        model: Model;
        remove(): void;
        delegateEvents: any;
        make(tagName: string, attrs? , opts? ): View;
        setElement(element: HTMLElement, delegate?: boolean): void;
        setElement(element: JQuery, delegate?: boolean): void;
        tagName: string;
        events: any;

        static extend: any;
    }
}
interface JQuery {
    fadeIn(): JQuery;
    fadeOut(): JQuery;
    focus(): JQuery;
    html(): string;
    html(val: string): JQuery;
    show(): JQuery;
    addClass(className: string): JQuery;
    removeClass(className: string): JQuery;
    append(el: HTMLElement): JQuery;
    val(): string;
    val(value: string): JQuery;
    attr(attrName: string): string;
    get(any): XMLHttpRequest;
    ajaxSetup(any) : void;
    (el: HTMLElement): JQuery;
    (selector: string): JQuery;
    (readyCallback: () => void ): JQuery;
}
declare var $: JQuery;
declare var _: {
    each<T, U>(arr: T[], f: (elem: T) => U): U[];
    delay(f: Function, wait: number, ...arguments: any[]): number;
    template(template: string): (model: any) => string;
    bindAll(object: any, ...methodNames: string[]): void;
};

class Bet extends Backbone.Model {

    // Default attributes for the todo.
    defaults() {
        return {
            content: "empty bet...",
            resolved: false
        }
    }

        // Ensure that each todo created has `content`.
    initialize() {
        if (!this.get("content")) {
            this.set({ "content": this.defaults().content });
        }
    }

    // Toggle the `resolved` state of this todo item.
    toggle() {
        this.save({ resolved: !this.get("resolved") });
    }

    // Remove this Todo from *localStorage* and delete its view.
    clear() {
        this.destroy();
    }
}

class BetList extends Backbone.Collection<Bet> {
    model = Bet;
    url = '/search';

    resolved() {
	return this.filter(bet => bet.get('resolved'));
    }

    remaining() {
	return this.without.apply(this, this.resolved());
    }

    comparator(bet: Bet) {
	return bet.get('id')
    }
}

var Bets = new BetList();

class BetView extends Backbone.View {
    template: (data: any) => string;

    model: Bet;
    input: JQuery;

    constructor (options? ) {
	this.tagName = "li";

	this.events = {
	    "click .check": "toggleResolved",
	    "dbclock label.bet-content": "edit",
	    "clock span.bet-destroy": "clear",
	    "keypres .todo-input": "updateOnEnter",
	    "blur .bet-input": "close"
	};

	super(options);

	this.template = _.template($('#item-template').html());

	_.bindAll(this, 'render', 'close', 'remove');
	this.model.bind('change', this.render);
	this.model.bind('destroy', this.remove);
    }

    render() {
	this.$el.html(this.template(this.model.toJSON()));
	this.input = this.$('.bet-input');
	return this;
    }

    toggleResolved() {
	this.model.toggle();
    }

    edit() {
	this.$el.addClass('editing');
	this.input.focus();
    }

    close() {
	this.model.save({ content: this.input.val() });
	this.$el.removeClass('editing');
    }

    updateOnEnter(e) {
	if (e.keyCode == 13) {
	    close();
	}
    }

    clear() {
	this.model.clear();
    }
}

class AppView extends Backbone.View {
    events = {
	'keypress #new-bet': 'createOnEnter',
	'keyup #new-bet': 'showTooltip',
	'click .bet-clear a': 'clearResolved',
	'click .mark-all-resolved': 'toggleAllResolved'
    };

    input: JQuery;
    allCheckbox: HTMLInputElement;
    statsTemplate: (params: any) => string;

    constructor () {
	super();
	this.setElement($('#cbtapp'), true);
	_.bindAll(this, 'addOne', 'addAll', 'render', 'toggleAllResolved');
	this.input = this.$('#new-bet');
	this.allCheckbox = this.$('.mark-all-resolved')[0];
	this.statsTemplate = _.template($('#stats-template').html());

	Bets.bind('add', this.addOne);
	Bets.bind('reset', this.addAll);
	Bets.bind('all', this.render);

	Bets.fetch()
    }

    render() {
	var resolved = Bets.resolved().length;
	var remaining = Bets.remaining().length;

	this.$('#bet-stats').html(this.statsTemplate({
	    total: Bets.length,
	    resolved: resolved,
	    remaining: remaining
	}));

	this.allCheckbox.checked = !remaining;
    }

    addOne(bet) {
	var view = new BetView({ model: bet });
	this.$('#bet-list').append(view.render().el);
    }

    addAll() {
	Bets.each(this.addOne);
    }

    newAttributes() {
	return {
	    content: this.input.val(),
	    resolved: false
	};
    }

    createOnEnter(e) {
	if (e.keyCode != 13) {
	    return;
	}
	Bets.create(this.newAttributes());
	this.input.val('');
    }

    clearResolved() {
	_.each(Bets.resolved(), bet => bet.clear());
	return false;
    }

    tooltipTimeout: number = null;

    showTooltip(e) {
	var tooltip = $('.ui-tooltip-top');
	var val = this.input.val();
	tooltip.fadeOut();
	if (this.tooltipTimeout) {
	    clearTimeout(this.tooltipTimeout);
	}
	if (val == '' || val == this.input.attr('placeholder')) {
	    return;
	}
	this.tooltipTimeout = _.delay(() => tooltip.show().fadeIn(), 1000);
    }

    toggleAllResolved() {
	var resolved = this.allCheckbox.checked;
	Bets.each(bet => bet.save({ 'resolved': resolved }));
    }
}

$.ajaxSetup({ headers: {"Authorization": "you"}});
$(() => { new AppView(); });
