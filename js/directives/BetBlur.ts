/// <reference path='../_all.ts' />

module bets {
    'use strict';

    /**
     * Directive that executes an expression when the element it is applied to loses focus.
     */
    export function betBlur(): ng.IDirective {
        return {
            link: ($scope: ng.IScope, element: JQuery, attributes: any) => {
                element.bind('blur', () => { $scope.$apply(attributes.betBlur); });
                $scope.$on('$destroy', () => { element.unbind('blur'); });
            }
        };
    }
}
