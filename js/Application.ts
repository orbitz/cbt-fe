/// <reference path='_all.ts' />
module bets {
    'use strict';

    var betmvc = angular.module('betsmvc', [])
            .controller('betCtrl', BetCtrl)
            .directive('betBlur', betBlur)
            .directive('betFocus', betFocus)
            .service('betStorage', BetStorage);
}
