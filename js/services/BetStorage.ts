/// <reference path='../_all.ts' />

module bets {
    'use strict';

    /**
     * Services that persists and retrieves TODOs from localStorage.
     */
    export class BetStorage implements IBetStorage {
	private bets: BetItem[];
	private http: ng.IHttpService;

	public static $inject = [
	    '$http',
	];

	constructor($http: ng.IHttpService) {
	    this.http = $http;
	    this.http.defaults.headers.common.Authorization = 'you';
	}

        get(): ng.IHttpPromise<{entries: BetItem[]}> {
	    return this.http.get('/search');
	}

        put(bets: BetItem[]) {
        }
    }
}