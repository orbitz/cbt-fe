/// <reference path='../_all.ts' />

module bets {
    'use strict';

    export class BetItem {
        constructor(
	    public id: string,
            public title: string,
	    public validation_data: string,
	    public comments: string[],
	    public tags: string[],
            public resolved: string,
	    public version: string
            ) { }
    }
}
