/// <reference path='../_all.ts' />

module bets {
    'use strict';

    /**
     * The main controller for the app. The controller:
     * - retrieves and persists the model via the todoStorage service
     * - exposes the model to the template and provides event handlers
     */
    export class BetCtrl {

	private bets: BetItem[];

	// $inject annotation.
	// It provides $injector with information about dependencies to be injected into constructor
	// it is better to have it close to the constructor, because the parameters must match in count and type.
	// See http://docs.angularjs.org/guide/di
	public static $inject = [
	    '$scope',
	    '$location',
	    'betStorage',
	    'filterFilter'
	];

	// dependencies are injected via AngularJS $injector
	// controller's name is registered in Application.ts and specified from ng-controller attribute in index.html
	constructor(
	    private $scope: IBetScope,
	    private $location: ng.ILocationService,
	    private betStorage: IBetStorage,
	    private filterFilter
	) {
	    this.bets = $scope.bets = [];
	    this.betStorage.get().success(entries => {
		var bets = entries.entries;
		this.bets = bets;
		$scope.bets = bets;
	    });
	    $scope.newBet = '';
	    $scope.editedBet = null;

	    // 'vm' stands for 'view model'. We're adding a reference to the controller to the scope
	    // for its methods to be accessible from view / HTML
	    $scope.vm = this;

	    // watching for events/changes in scope, which are caused by view/user input
	    // if you subscribe to scope or event with lifetime longer than this controller, make sure you unsubscribe.
	    $scope.$watch('bets', () => this.onBets(), true);
	    $scope.$watch('location.path()', path => this.onPath(path))

	    if ($location.path() === '') $location.path('/');
	    $scope.location = $location;
	}

	onPath(path: string) {
	    this.$scope.statusFilter = (path === '/active') ?
		(bet, index) => bet.resolved === 'Unresolved' : (path === '/resolved') ?
		(bet, inde) => bet.resolved !== 'Unresolved' : null;
	}

	onBets() {
	    this.$scope.remainingCount = this.filterFilter(this.bets, { resolved: 'Unresolved' }).length;
	    this.$scope.resolvedCount = this.bets.length - this.$scope.remainingCount;
	    this.$scope.allChecked = !this.$scope.remainingCount
	    this.betStorage.put(this.bets);
	}

	addBet() {
	    var newBet : string = this.$scope.newBet.trim();
	    if (!newBet.length) {
		return;
	    }

	    this.bets.push(new BetItem(newBet, "Unresolved"));
	    this.$scope.newBet = '';
	}

	editBet(betItem: BetItem) {
	    this.$scope.editedBet = betItem;
	}

	doneEditing(betItem: BetItem) {
	    this.$scope.editedBet = null;
	    betItem.title = betItem.title.trim();
	    if (!betItem.title) {
		this.removeBet(betItem);
	    }
	}

	removeBet(betItem: BetItem) {
		    this.bets.splice(this.bets.indexOf(betItem), 1);
	}

	clearResolvedBets() {
	    this.$scope.bets = this.bets = this.bets.filter(betItem => !betItem.resolved);
	}

	markAll(resolved: string) {
	    this.bets.forEach(betItem => { betItem.resolved = resolved; });
	}
    }
}
